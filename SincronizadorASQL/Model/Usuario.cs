﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;


namespace SincronizadorASQL.Model
{
    public class Usuario
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { set; get; }
        public string Name { set; get; }
        public string Password { set; get; }
        public DateTime Create_date { set; get; }
        public DateTime Update_date { set; get; }

    }

    public class UsuarioContext : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=SincronizadorASQL.UsariosDb;Trusted_Connection=True;");
        }
    }   

}
