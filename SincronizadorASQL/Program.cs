﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SincronizadorASQL.Model;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Newtonsoft.Json;

namespace SincronizadorASQL
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var factory = new ConnectionFactory() { HostName = "localhost" };
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "sincronizador",
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);
                        List<Usuario> datos = JsonConvert.DeserializeObject<List<Usuario>>(message);
                        Console.WriteLine(" [x] Sincronizacion recibida");
                        SincronizarDatos(datos);
                        Console.WriteLine(" [x] Datos Actualizados");
                        
                    };
                    channel.BasicConsume(queue: "sincronizador",
                                         autoAck: true,
                                         consumer: consumer);

                    Console.WriteLine("Presionar [Enter] para salir");
                    Console.ReadLine();
                }

            


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);                
            }
        }

        static void SincronizarDatos(List<Usuario> newdata)
        {
            try
            {

                using (var db = new UsuarioContext())
                {
                    List<Usuario> old = db.Usuarios.ToList<Usuario>();
                    foreach (Usuario item in old)
                    {
                        var delete = newdata.Where(x => x.ID == item.ID).ToList();
                        if (delete.Count == 0)
                        {
                            // Borrar item de la BD
                            db.Usuarios.Remove(item);
                            db.SaveChanges();
                            continue;
                        }
                        else
                        {
                            var update = newdata.Where(x => x.ID == item.ID && x.Update_date > item.Update_date).ToList();
                            if (update.Count > 0)
                            {
                                // Actualizar elementto
                                db.Usuarios.Update(item);
                                db.SaveChanges();
                                continue;
                            }
                        }

                    }
                    old = db.Usuarios.ToList<Usuario>();
                    List<Usuario> newelements = newdata.Where(x => !old.Any(y => y.ID == x.ID)).ToList();
                    if (newelements.Count > 0)
                    {
                        foreach (Usuario item in newelements)
                        {
                            db.Usuarios.Add(item);
                            db.SaveChanges();
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
    //https://docs.microsoft.com/en-us/ef/core/get-started/full-dotnet/new-db
}
